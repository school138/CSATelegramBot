package com.telegram.bot.TelegramBot;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


public class CSATelegramBOT extends TelegramLongPollingBot {

    private String welcomeText = "Hallo Ich bin das TelegramBot Projekt für Client Server Architekturen.\n" +
            "\n" +
            "Wie kann ich dir helfen?";

    private String infoText = "Für die Projektarbeit haben wir diesen Bot erstellt.";

    private String endText = "So, wir sind am Ende angelangt. Wir bedanken uns für eure Aufmerksamkeit und falls ihr noch Fragen habt, könnt ihr diese jetzt stellen.";

    private String commandNotSupportedText = "Ups, dieser Command wird nicht unterstützt.";

    @Override
    public String getBotUsername() {
        return "csatelegrambot";
    }

    @Override
    public String getBotToken() {
        return "herecomesyourtoken";
    }

    @Override
    public void onUpdateReceived(Update update) {
        String userInput = update.getMessage().getText();
        SendMessage message = new SendMessage();

        //Hier wird die Antwort des Bots anhand des userInputs gesetzt
        if (userInput.equals("/start")){
            message.setText(welcomeText);
        } else if (userInput.equals("️️ℹ️")) {
            message.setText(infoText);
        } else if (userInput.equals("👋")) {
            message.setText(endText);
        } else {
            message.setText(commandNotSupportedText);
        }

        //Hier wird der Chat gesetzt.
        message.setChatId(update.getMessage().getChatId());

        //Hier wird der Keyboard gesetzt, welches euch die Emojis anzeigt
        message.setReplyMarkup(getStandardKeyboard());
        try {
            //Die Antwort des Bots wird hier versendet.
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    //Hier wird der Keyboard erstellt.
    public static ReplyKeyboardMarkup getStandardKeyboard() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add("️️ℹ️");
        keyboardFirstRow.add("👋");
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }
}
